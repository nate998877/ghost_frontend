import React, { Component } from 'react';
import { Post } from '../components'

const API_HOST = 'http://localhost:8000';

let _csrfToken = null;

async function getCsrfToken() {
  if (_csrfToken === null) {
    const response = await fetch(`${API_HOST}/csrf/`, {
      credentials: 'include',
    });
    const data = await response.json();
    _csrfToken = data.csrfToken;
  }
  return _csrfToken;
}

async function testRequest(method) {
  const response = await fetch(`${API_HOST}/ping/`, {
    method: method,
    headers: (
      method === 'POST'
        ? {'X-CSRFToken': await getCsrfToken()}
        : {}
    ),
    credentials: 'include',
  });
  const data = await response.json();
  return data.result;
}


export default class Feed extends Component{
  state = {data:[]}

  async componentDidMount(){
    fetch(`${API_HOST}/broast`)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      this.setState({ data })
    })
    .catch(e => console.log(e))
  }

  toggleBroast = i => () => {
    this.setState({post:i})
  }

  
  render(){
    let posts = []
    if(this.state.post === "Boast"){
      posts = this.state.data.filter(x => x.boast_or_roast)
    }else if(this.state.post === "Roast"){
      posts = this.state.data.filter(x => !x.boast_or_roast)
    } else {
      posts = this.state.data
    }
    return(
      <div>
        <div>
        <button onClick={this.toggleBroast(null)}>All posts</button>
        <button onClick={this.toggleBroast("Boast")}>Boast</button>
        <button onClick={this.toggleBroast("Roast")}>Roast</button>
        </div>
        <br/>
        {posts.map(x => <Post message={x} />)}
        <br/>
        <br/>
        <a href='/post'> Add a broast</a>
      </div>

    )
  }
}