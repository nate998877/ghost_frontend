import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
const API_HOST = 'http://localhost:8000';


let _csrfToken = null;

async function getCsrfToken() {
  if (_csrfToken === null) {
    const response = await fetch(`${API_HOST}/csrf/`, {
      credentials: 'include',
    });
    const data = await response.json();
    _csrfToken = data.csrfToken;
  }
  return _csrfToken;
}


export default class PostForm extends Component{
  state = {boast_or_roast:false, content:""}
  
  createPost = async e => {
    let formdata = new FormData()
    for(let key in this.state){
      console.log(key, this.state[key])
      formdata.append(key, this.state[key])
    }
    const token = await getCsrfToken()
    formdata.append('csrfmiddlewaretoken', token)
    fetch(`${API_HOST}/broast/`,{
      method: 'POST',
      body: formdata
    }).then(x =>{
      this.setState({redirect:true})
    })
  }



  handleChange = e => {
      this.setState({ [e.target.name]: e.target.value });
  };

  handleButton = () => {
    this.setState({boast_or_roast:!this.state.boast_or_roast})
  }



  render(){
    return(
      <React.Fragment>
        <form>
          <label>
            Post Type:
            <div>
              <input type='radio' onClick={this.handleButton} checked={!this.state.boast_or_roast}/> Roast
              <input type='radio' onClick={this.handleButton} checked={this.state.boast_or_roast}/> Boast
            </div>
          </label>
          <label>
            Content:
            <input  type='text' name='content' onChange={this.handleChange} />
          </label>
          <br/>
          <input type="button" value='Submit' onClick={this.createPost}/>
        </form>
        {this.state?.redirect ? <Redirect exact to='/' /> : ""}
      </React.Fragment>
    )
  }
}