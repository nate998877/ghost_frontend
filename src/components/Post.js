import React, { Component } from 'react';

const API_HOST = 'http://localhost:8000';

export default class Post extends Component {

  upvote = () => {
    fetch(`${API_HOST}/broast/${this.props.message.id}/upvote/`,{method: 'POST'})
    .then(res => res.json())
    .then(data => {
      console.log(data)
      this.setState({ upvote:data.upvotes })
    })
    .catch(e => console.log(e))
  }

  downvote = () => {
    fetch(`${API_HOST}/broast/${this.props.message.id}/downvote/`,{
      method: 'POST'
    }
    )
    .then(res => res.json())
    .then(data => {
      console.log(data)
      this.setState({ downvote:data.downvotes })
    })
    .catch(e => console.log(e))
  }


  render(){
    return(
      <React.Fragment>
      {this.props.message.boast_or_roast ? "Boast" : "Roast"}:
      <br />
      {this.props.message.content}
      <br />
      <button onClick={this.upvote}>upvote</button> : {this.state?.upvote ? this.state.upvote : this.props.message.upvote }
      <br />
      <button onClick={this.downvote}>downvote</button> : {this.state?.downvote ? this.state.downvote : this.props.message.downvote}
      <br />
      </React.Fragment>
    )
  }
}