import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import './App.css';
import { Feed, PostForm } from './pages'
import { createBrowserHistory } from "history";

const history = createBrowserHistory();


function App() {
  return (
    <Router history={history}>
      <Switch>
        <Route exact path="/" render={() => <Feed />} />
        <Route exact path="/post" render={() => <PostForm />} />
      </Switch>
    </Router>
  );
}

export default App;
